package com.ihstockserver.controller.news;

import com.ihstockserver.model.news.dto.News;
import com.ihstockserver.model.news.service.NewsService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/news")
public class NewsController {
	static final Logger logger = LogManager.getLogger(NewsController.class.getName());
	@Autowired
	private NewsService newsService;

	@RequestMapping(method = RequestMethod.GET)
	public String welcomeNews(ModelMap modelMap) {
		modelMap.addAttribute("title", "NEWS - ihsong");
		modelMap.addAttribute("name", "Hellow World");
		modelMap.addAttribute("greetings", "welcom to ih publishing - spring MVC");
		return "news";
	}

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String listNews(ModelMap modelMap) {
		List<News> list = newsService.getAllNews("0000000");
		logger.debug(list);
		logger.debug(list.toArray());
		modelMap.addAttribute("title", "NEWS - ihsong");
		modelMap.addAttribute("list", list);

//		ApplicationContext context = new AnnotationConfigApplicationContext("com.ihstockserver.model");
//	  newsService = new NewsServiceImpl();
//		newsService = context.getBean(NewsServiceImpl.class);
//		modelMap.addAttribute("list", newsService.getAllNewss("0000000"));
		return "list";
	}

//  @RequestMapping(value = "add_news",method = RequestMethod.GET)
//  public String getAddEmployee(ModelMap modelMap) {
//    return "add_news";
//  }

  @RequestMapping(value = "add_news",method = RequestMethod.GET)
  public ModelAndView addEmployee(ModelMap modelMap) {
    return new ModelAndView("add_news", "command", new News());
  }

	@RequestMapping(value = "update_news",method = RequestMethod.POST)
	public String updateEmployee(@ModelAttribute("newsForm") News news, ModelMap modelMap) {
		this.newsService.insertNews(news);
		modelMap.addAttribute("list", this.newsService.getAllNews("0000000"));
		return "list";
	}

	@RequestMapping(value = "delete_news/{newsId}",method = RequestMethod.GET)
	public String deleteEmployee(@PathVariable("newsId") Integer newsId, ModelMap modelMap) {
		this.newsService.deleteNews(newsId);
		modelMap.addAttribute("list", this.newsService.getAllNews("0000000"));
		return "list";
	}
}
