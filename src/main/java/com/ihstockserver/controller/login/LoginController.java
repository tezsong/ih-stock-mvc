package com.ihstockserver.controller.login;

import com.ihstockserver.model.news.dto.News;
import com.ihstockserver.model.news.service.NewsService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {
	static final Logger logger = LogManager.getLogger(LoginController.class.getName());
	@Autowired
	private NewsService newsService;

	@RequestMapping(method = RequestMethod.GET)
	public String welcomeLogin(ModelMap modelMap) {
		return "login";
	}

}
