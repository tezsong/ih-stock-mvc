package com.ihstockserver.controller.ticker;

import com.ihstockserver.model.ticker.dto.Ticker;
import com.ihstockserver.model.ticker.service.TickerService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/ticker")
public class TickerController {
	static final Logger logger = LogManager.getLogger(TickerController.class.getName());
	@Autowired
	private TickerService tickerService;

	@RequestMapping(method = RequestMethod.GET)
	public String welcomeTicker(ModelMap modelMap) {
		modelMap.addAttribute("name", "Hellow World");
		modelMap.addAttribute("greetings", "welcom to ih publishing - spring MVC");
		return "hello";
	}

	@RequestMapping(value="list", method = RequestMethod.GET)
	public String listTicker(ModelMap modelMap) {
		List<Ticker> list = tickerService.getAllTickers("0000000");
		logger.debug(list);
		modelMap.addAttribute("list", list);
		modelMap.addAttribute("ticker", list.toArray()[0]);
		logger.debug(list.toArray());

//		ApplicationContext context = new AnnotationConfigApplicationContext("com.ihstockserver.model");
//	  tickerService = new NewsServiceImpl();
//		tickerService = context.getBean(NewsServiceImpl.class);
//		modelMap.addAttribute("list", tickerService.getAllTickers("0000000"));
		return "list";
	}
}
