package com.ihstockserver.model.ticker.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class TickerDBUtils {
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void initialize() {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Ticker(" +
//					"id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
					"market VARCHAR(10) NOT NULL, " +
					"code VARCHAR(10) NOT NULL, " +
					"date CHAR(10) NOT NULL," +
					"close BIGINT(20) NOT NULL, " +
					"high BIGINT(20) NOT NULL, " +
					"low BIGINT(20) NOT NULL, " +
					"open BIGINT(20) NOT NULL, " +
					"volume BIGINT(20) NOT NULL, " +
					"PRIMARY KEY (code, date), " +
					"INDEX market (market)" +
					")");

			statement.close();
			connection.close();
		} catch(SQLException err) {
			err.printStackTrace();
		}
	}
}
