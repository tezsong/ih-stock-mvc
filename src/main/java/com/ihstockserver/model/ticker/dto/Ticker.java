package com.ihstockserver.model.ticker.dto;

import javax.persistence.*;
import java.io.Serializable;

//import java.util.Date;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;

class TickerPK implements Serializable {
	protected String code;
	protected String date;

	@Override
	public int hashCode() {
//		return code != null ? code.hashCode() : 0;
		String sum = code+'-'+date;
		return sum.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TickerPK)) {
			return false;
		}
		TickerPK ticker = (TickerPK) obj;
		if (code != null ? !code.equals(ticker.code) : ticker.code != null) {
			return false;
		} else {
			if (date != null ? !date.equals(ticker.date) : ticker.date != null) {
				return false;
			} else {
				return true;
			}
		}
	}
}

@Entity
@Table
@IdClass(TickerPK.class)
public class Ticker {
	private String market;

	@Id
	private String date;
	@Id
	private String code;

	private Integer close;
	private Integer high;
	private Integer low;
	private Integer open;
	private Integer volume;

//	public void setId(Integer id) {
//		this.id = id;
//	}
//	public Integer getId() {
//		return id;
//	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getMarket() {
		return market;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setDate(String dateData) {
		this.date = dateData;
	}
	public String getDate() {
		return date;
	}
	public void setClose(Integer close) {
		this.close = close;
	}
	public Integer getClose() {
		return close;
	}
	public void setLow(Integer low) {
		this.low = low;
	}
	public Integer getLow() {
		return low;
	}
	public void setHigh(Integer high) {
		this.high = high;
	}
	public Integer getHigh() {
		return high;
	}
	public void setOpen(Integer open) {
		this.open = open;
	}
	public Integer getOpen() {
		return open;
	}
	public void setVolume(Integer volume) {
		this.volume = volume;
	}
	public Integer getVolume() {
		return volume;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Ticker)) {
			return false;
		}
		Ticker ticker = (Ticker) obj;
		if (code != null ? !code.equals(ticker.code) : ticker.code != null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		return code != null ? code.hashCode() : 0;
//		String sum = market+'-'+code+'-'+date;
//		return sum.hashCode();
	}

	@Override
	public String toString() {
		return "Ticker [" +
//				"id=" + id + ", " +
				"market=" + market + ", " +
				"code=" + code + ", " +
				"date=" + date + ", " +
				"close=" + close + ", " +
				"high=" + high + ", " +
				"low=" + low + ", " +
				"open=" + open + ", " +
				"volume=" + volume + "]";
	}
}
