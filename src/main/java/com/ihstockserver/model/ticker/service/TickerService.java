package com.ihstockserver.model.ticker.service;

import com.ihstockserver.model.ticker.dto.Ticker;

import java.util.List;

public interface TickerService {
	public List<Ticker> getAllTickers(String code);
	public List<Ticker> getTickers(String code, Integer perPage, Integer index);
	public void insertTicker(Ticker ticker);
}
