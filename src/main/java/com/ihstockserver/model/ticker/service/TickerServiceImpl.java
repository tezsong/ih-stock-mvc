package com.ihstockserver.model.ticker.service;

import com.ihstockserver.model.ticker.dao.TickerDao;
import com.ihstockserver.model.ticker.dto.Ticker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TickerServiceImpl implements TickerService {
	@Autowired
	private TickerDao tickerDao;

	public List<Ticker> getAllTickers(String code) {
		List<Ticker> tickerList = tickerDao.getAllTickers(code);
		return tickerList;
	}

	public List<Ticker> getTickers(String code, Integer perPage, Integer index) {
		List<Ticker> tickerList = tickerDao.getTickers(code, perPage, index);
		return tickerList;
	}

	public void insertTicker(Ticker ticker) {
		tickerDao.insertTicker(ticker);
	}
}
