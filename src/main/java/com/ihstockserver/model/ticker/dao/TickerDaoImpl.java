package com.ihstockserver.model.ticker.dao;

import com.ihstockserver.model.ticker.dto.Ticker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class TickerDaoImpl implements TickerDao {
	static final Logger logger = LogManager.getLogger(TickerDaoImpl.class.getName());
	@Autowired()
	private SessionFactory sessionFactory;
	private DateFormat dateFormat;
	CriteriaQuery<Ticker> prebuildCriteria;

	public TickerDaoImpl() {
		dateFormat = new SimpleDateFormat("yyyy-mm-dd");
//		ApplicationContext context = new AnnotationConfigApplicationContext("org.springframework.orm.hibernate5.LocalSessionFactoryBean");
//
//		sessionFactory = context.getBean("org.springframework.orm.hibernate5.LocalSessionFactoryBean")
	}

	@SuppressWarnings("unchecked")
	public List<Ticker> getAllTickers(String code) {
		Session session = sessionFactory.openSession();
		String hql = "FROM Ticker WHERE code = :codeName ORDER BY date DESC";
		Query query = session.createQuery(hql);
		query.setParameter("codeName", code);
		List<Ticker> tickerList = query.list();
		return tickerList;
	}

	@SuppressWarnings("unchecked")
	public List<Ticker> getTickers(String code, Integer perPage, Integer index) {
		Session session = sessionFactory.openSession();
		String hql = "FROM Ticker WHERE code = :codeName ORDER BY date ASC";
		Query query = session.createQuery(hql);
		query.setParameter("codeName", code);
		query.setMaxResults(perPage);
		query.setFirstResult(index);
		List<Ticker> tickerList = query.list();
		return tickerList;
	}

	@Transactional(readOnly=false)
	public void insertTicker(Ticker ticker) {
		try {
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.save(ticker);
			t.commit();
			session.close();
			logger.debug("insert: "+ticker);
		} catch (HibernateException err) {
			err.printStackTrace();
		}
	}
}
