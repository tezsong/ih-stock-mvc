package com.ihstockserver.model.ticker.dao;

import com.ihstockserver.model.ticker.dto.Ticker;

import java.util.List;

public interface TickerDao {
	public List<Ticker> getAllTickers(String code);
	public List<Ticker> getTickers(String code, Integer perPage, Integer index);
	public void insertTicker(Ticker ticker);
}
