package com.ihstockserver.model.news.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class NewsDBUtils {
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void initialize() {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS news(" +
				"id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
				"code VARCHAR(10) NOT NULL, " +
				"date CHAR(10) NOT NULL" +
				")"
			);

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS sentence(" +
				"id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
				"code VARCHAR(10) NOT NULL, " +
				"date CHAR(10) NOT NULL," +
				"sentence VARCHAR(1000) NOT NULL," +
				"news_id BIGINT(20) NOT NULL, " +
				"INDEX FK_NEWS (news_id)," +
				"CONSTRAINT FK_NEWS FOREIGN KEY (news_id) REFERENCES news (id)" +
				")");

			statement.close();
			connection.close();
		} catch(SQLException err) {
			err.printStackTrace();
		}
	}
}
