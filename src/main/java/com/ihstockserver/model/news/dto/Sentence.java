package com.ihstockserver.model.news.dto;

import javax.persistence.*;
//	@ManyToOne(optional=false)
//	@JoinColumn(name="news_id", referencedColumnName="id",
//		foreignKey = @ForeignKey(name = "FK_NEWS"))

@Entity
@Table
public class Sentence {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String sentence;
	private String code;
	private String date;

	@ManyToOne
	private News news;

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
	public String getSentence() {
		return sentence;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}
  public void setNews(News news) {
    this.news = news;
  }
  public News getNews() {
    return news;
  }

  @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Sentence)) {
			return false;
		}
		Sentence sentence = (Sentence) obj;
		if (id != null ? !id.equals(sentence.id) : sentence.id != null) {
			return false;
		} else if (sentence != null ? !sentence.equals(sentence.sentence) : sentence.sentence != null) {
			return false;
		} else if (code != null ? !code.equals(sentence.code) : sentence.code != null) {
			return false;
		} else if (date != null ? !date.equals(sentence.date) : sentence.date != null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
//		String sum = market+'-'+code+'-'+date;
//		return sum.hashCode();
	}

	@Override
	public String toString() {
		return "Sentence [" +
			"id=" + id + ", " +
			"sentence=" + sentence + ", " + "]";
	}
}
