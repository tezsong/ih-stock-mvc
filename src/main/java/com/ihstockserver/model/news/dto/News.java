package com.ihstockserver.model.news.dto;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class News {
  static final Logger logger = LogManager.getLogger(News.class.getName());
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String code;
	private String date;

	@OneToMany(mappedBy = "news", cascade=CascadeType.ALL, orphanRemoval = true)
	private List<Sentence> sentences = new ArrayList<Sentence>();
//	@Column()
//	private User ticker;

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}

	public void setSentences(List<Sentence> sentences) {
		this.sentences = sentences;
	}
	public List<Sentence> getSentences() {
		return sentences;
	}
	public List<Sentence> insertSentence(String message) {
		Sentence sentence = new Sentence();
		sentence.setSentence(message);
		sentence.setCode(code);
		sentence.setDate(date);
		sentence.setNews(this);
		sentences.add(sentence);
		return sentences;
	}
	public List<Sentence> removeSentence(String message) {
		for(Sentence sentence: sentences) {
			if (sentence.getSentence().equals(message)) {
				sentences.remove(sentence);
				return sentences;
			}
		}
		logger.debug("not such that message");
		return sentences;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof News)) {
			return false;
		}
		News news = (News) obj;
		if (id != null ? !id.equals(news.id) : news.id != null) {
			return false;
		} else if (code != null ? !code.equals(news.code) : news.code != null) {
			return false;
		} else if (date != null ? !date.equals(news.date) : news.date != null) {
			return false;
    } else if (sentences != null ? sentences.size() != news.sentences.size() : news.sentences != null) {
      return false;
    } else if (sentences == null && news.sentences == null) {
      return false;
		} else {
		  for(Integer i=0; i < sentences.size(); i++ ) {
		    if (sentences.get(i).equals(news.sentences.get(i))) {
		      return false;
        }
      }
			return true;
		}
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
//		String sum = market+'-'+code+'-'+date;
//		return sum.hashCode();
	}

	@Override
	public String toString() {
		String result = "News [" +
			"id=" + id + ", sentences[";
		for (Sentence sentence: sentences) {
			result += sentence + ", ";
		}
		result += "]]";
		return result;
	}
}
