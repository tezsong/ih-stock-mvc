package com.ihstockserver.model.news.dao;

import com.ihstockserver.model.news.dto.News;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class NewsDaoImpl implements NewsDao {
	static final Logger logger = LogManager.getLogger(NewsDaoImpl.class.getName());
	
	@Autowired()
	private SessionFactory sessionFactory;
	private DateFormat dateFormat;
	CriteriaQuery<News> prebuildCriteria;

	public NewsDaoImpl() {
		dateFormat = new SimpleDateFormat("yyyy-mm-dd");
	}

	@SuppressWarnings("unchecked")
	public List<News> getAllNews(String code) {
		Session session = sessionFactory.openSession();
		String hql = "FROM News WHERE code = :codeName ORDER BY date DESC";
		Query query = session.createQuery(hql);
		query.setParameter("codeName", code);
		List<News> newsList = query.list();
		return newsList;
	}

	@SuppressWarnings("unchecked")
	public News getNews(Integer newsId) {
		Session session = sessionFactory.openSession();
		String hql = "FROM News WHERE id = :idName ORDER BY date DESC";
		Query query = session.createQuery(hql);
		query.setParameter("idName", newsId);
		News news = (News)query.getSingleResult();
		return news;
	}

	@SuppressWarnings("unchecked")
	public List<News> getNews(String code, Integer perPage, Integer index) {
		Session session = sessionFactory.openSession();
		String hql = "FROM News WHERE code = :codeName ORDER BY date ASC";
		Query query = session.createQuery(hql);
		query.setParameter("codeName", code);
		query.setMaxResults(perPage);
		query.setFirstResult(index);
		List<News> newsList = query.list();
		logger.debug(newsList.toArray()[0]);
		return newsList;
	}

	@Transactional(readOnly=false)
	public void insertNews(News news) {
		try {
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.save(news);
			t.commit();
			session.close();
			logger.debug("insert: "+news);
		} catch (HibernateException err) {
			err.printStackTrace();
		}
	}

  @Transactional(readOnly=false)
  public void updateNews(News news) {
    try {
      Session session = sessionFactory.openSession();
      Transaction t = session.beginTransaction();
      session.update(news);
      t.commit();
      session.close();
      logger.debug("insert: "+news);
    } catch (HibernateException err) {
      err.printStackTrace();
    }
  }

	@Transactional(readOnly=false)
	public void deleteNews(News news) {
		try {
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.delete(news);
			t.commit();
			session.close();
			logger.debug("insert: "+news);
		} catch (HibernateException err) {
			err.printStackTrace();
		}
	}

	@Transactional(readOnly=false)
	public void deleteNews(Integer newsId) {
		try {
			News news = new News();
			news.setId(newsId);
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.delete(news);
			t.commit();
			session.close();
			logger.debug("insert: "+news);
		} catch (HibernateException err) {
			err.printStackTrace();
		}
	}

	@Transactional(readOnly=false)
	public void test(News news) {
		try {
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();

      String hql = "FROM News WHERE code = '0000000' ORDER BY date DESC";
      Query query = session.createQuery(hql);
      News c = (News)query.getSingleResult();
      c.removeSentence("2016년 전 세계 리튬이차전지용 양극소재 출하량은 약 215,000톤 가량이며,");
			session.persist(c);
			t.commit();
			session.close();
			logger.debug("execute: "+c);
		} catch (HibernateException err) {
			err.printStackTrace();
		}
	}
}
