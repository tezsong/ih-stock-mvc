package com.ihstockserver.model.news.dao;

import com.ihstockserver.model.news.dto.News;

import java.util.List;

public interface NewsDao {
	public List<News> getAllNews(String code);
	public List<News> getNews(String code, Integer perPage, Integer index);

	public News getNews(Integer newsId);
	public void insertNews(News news);
  public void updateNews(News news);
	public void deleteNews(News news);
	public void deleteNews(Integer newsId);

	public void test(News news);
}
