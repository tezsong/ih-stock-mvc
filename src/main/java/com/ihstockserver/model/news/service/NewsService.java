package com.ihstockserver.model.news.service;

import com.ihstockserver.model.news.dto.News;

import java.util.List;

public interface NewsService {
	public List<News> getAllNews(String code);
	public List<News> getNews(String code, Integer perPage, Integer index);
	public News getNews(Integer newsId);
	public void insertNews(News news);
	public void deleteNews(Integer newsId);

	public void test(News news);
}
