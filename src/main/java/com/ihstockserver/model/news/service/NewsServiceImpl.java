package com.ihstockserver.model.news.service;

import com.ihstockserver.model.news.dao.NewsDao;
import com.ihstockserver.model.news.dto.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
  @Autowired
  private NewsDao newsDao;

  public NewsServiceImpl(NewsDao newsDao) {
    this.newsDao = newsDao;
  }

  public List<News> getAllNews(String code) {
    List<News> newsList = newsDao.getAllNews(code);
    return newsList;
  }

  public List<News> getNews(String code, Integer perPage, Integer index) {
    List<News> newsList = newsDao.getNews(code, perPage, index);
    return newsList;
  }
  public News getNews(Integer newsId) {
    News news = newsDao.getNews(newsId);
    return news;
  }
  public void insertNews(News news) {
    newsDao.insertNews(news);
  }
  public void deleteNews(Integer newsId) {
    newsDao.deleteNews(newsId);
  }

  public void test(News news) {
    newsDao.test(news);
  }
}
