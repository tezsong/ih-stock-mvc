package com.ihstockserver.model.user.dao;

import com.ihstockserver.model.user.dto.User;
import com.ihstockserver.model.user.dto.User_authorization;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public class UserDaoImpl implements UserDao {
	static final Logger logger = LogManager.getLogger(UserDaoImpl.class.getName());
	@Autowired()
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public User login(String username, String password) {
		Session session = sessionFactory.openSession();
		String hql = "FROM User WHERE " +
				"username = :username and " +
				"password = :password" +
				" ORDER BY updatedAt DESC";
		Query query = session.createQuery(hql);
		query.setParameter("username", username);
		query.setParameter("password", password);
		User user = (User)query.getSingleResult();
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		Session session = sessionFactory.openSession();
		String hql = "FROM User ORDER BY updatedAt DESC";
		Query query = session.createQuery(hql);
		List<User> userList = query.list();
		return userList;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers(Integer perPage, Integer index) {
		Session session = sessionFactory.openSession();
		String hql = "FROM User ORDER BY updatedAt ASC";
		Query query = session.createQuery(hql);
		query.setMaxResults(perPage);
		query.setFirstResult(index);
		List<User> userList = query.list();
		logger.debug(userList.toArray()[0]);
		return userList;
	}

	@SuppressWarnings("unchecked")
	public User getUser(Integer id) {
		Session session = sessionFactory.openSession();
		String hql = "FROM User WHERE id = :idName ORDER BY updatedAt ASC";
		Query query = session.createQuery(hql);
		query.setParameter("idName", id);
		List<User> userList = query.list();
		logger.debug(userList.toArray()[0]);
		return userList.get(0);
	}

	@SuppressWarnings("unchecked")
	public User getUser(String username) {
		Session session = sessionFactory.openSession();
		String hql = "FROM User WHERE username = :usernameName ORDER BY updatedAt ASC";
		Query query = session.createQuery(hql);
		query.setParameter("usernameName", username);
		List<User> userList = query.list();
		logger.debug(userList.toArray()[0]);
		return userList.get(0);
	}

  @Transactional(readOnly=false)
  public void insertUser(User user) {
    try {
      Session session = sessionFactory.openSession();
      Transaction t = session.beginTransaction();
      session.save(user);
      t.commit();
      session.close();
      logger.debug("insert User: "+user);
    } catch (HibernateException err) {
      err.printStackTrace();
    }
  }

  @Transactional(readOnly=false)
  public void insertUser(User user, List<User_authorization> user_authorizations) {
    try {
      Session session = sessionFactory.openSession();
      Transaction t = session.beginTransaction();
      session.save(user);
      t.commit();
			for(User_authorization user_authorization: user_authorizations) {
			  user_authorization.setId(user.getId());
			  session.save(user_authorization);
      }
      session.close();
      logger.debug("insert User: "+user);
    } catch (HibernateException err) {
      err.printStackTrace();
    }
  }

//	@Transactional(readOnly=false)
//	public void updateUser(User user) {
//		try {
//			Session session = sessionFactory.openSession();
//			Transaction t = session.beginTransaction();
//			session.update(user);
//			t.commit();
//			session.close();
//			logger.debug("insert: "+user);
//		} catch (HibernateException err) {
//			err.printStackTrace();
//		}
//	}
}
