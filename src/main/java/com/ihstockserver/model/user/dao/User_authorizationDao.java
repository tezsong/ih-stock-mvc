package com.ihstockserver.model.user.dao;

import com.ihstockserver.model.user.dto.User_authorization;

import java.util.List;

public interface User_authorizationDao {
  public void insertUser_authorization(User_authorization user_authorization);
  public List<User_authorization> getUser_authorization(Integer id);
}
