package com.ihstockserver.model.user.dao;

import com.ihstockserver.model.user.dto.User_authorization;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public class User_authorizationDaoImpl implements User_authorizationDao {
  static final Logger logger = LogManager.getLogger(User_authorizationDaoImpl.class.getName());
  @Autowired()
  private SessionFactory sessionFactory;

  @Transactional(readOnly=false)
  public void insertUser_authorization(User_authorization user_authorization) {
    try {
      Session session = sessionFactory.openSession();
      Transaction t = session.beginTransaction();
      session.save(user_authorization);
      t.commit();
      session.close();
      logger.debug("insert: "+user_authorization);
    } catch (HibernateException err) {
      err.printStackTrace();
    }
  }

  public List<User_authorization> getUser_authorization(Integer id) {
    Session session = sessionFactory.openSession();
    String hql = "FROM User_authorization WHERE " +
        "id = :id " +
        " ORDER BY updatedAt DESC";
    Query query = session.createQuery(hql);
    query.setParameter("id", id);
    List<User_authorization> userList = query.list();
    return userList;
  }
}
