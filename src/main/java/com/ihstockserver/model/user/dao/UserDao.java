package com.ihstockserver.model.user.dao;

import com.ihstockserver.model.user.dto.User;
import com.ihstockserver.model.user.dto.User_authorization;

import java.util.List;

public interface UserDao {
	public User login(String username, String password);
	public List<User> getAllUsers();
	public List<User> getUsers(Integer perPage, Integer index);
	public User getUser(Integer id);
	public User getUser(String username);
	public void insertUser(User user);
	public void insertUser(User user, List<User_authorization> user_authorizations);
//	public void updateUser(User user);
}
