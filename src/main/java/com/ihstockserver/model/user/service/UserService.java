package com.ihstockserver.model.user.service;

import com.ihstockserver.model.user.dto.User;

import java.util.List;

public interface UserService {
	public boolean login(String username, String password);
	public void logout();
	public List<User> getAllUsers();
	public List<User> getUsers(Integer perPage, Integer index);
	public User getUser(Integer id);
	public User getUser(String username);
	public void insertUser(User user);
}
