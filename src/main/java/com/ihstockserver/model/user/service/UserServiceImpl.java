package com.ihstockserver.model.user.service;

import com.ihstockserver.model.user.dao.UserDao;
import com.ihstockserver.model.user.dao.User_authorizationDao;
import com.ihstockserver.model.user.dto.CurrentUser;
import com.ihstockserver.model.user.dto.User;
import com.ihstockserver.model.user.dto.User_authorization;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
	static final Logger logger = LogManager.getLogger(UserServiceImpl.class.getName());
	@Autowired
	private UserDao userDao;
	@Autowired
	private User_authorizationDao user_authorizationDao;

	public boolean login(String username, String password) {
		User user = userDao.login(username, password);
		if(user.getUsername() != null) {
			logger.debug("logged in with:"+user);
			CurrentUser.logIn(user.getUsername());
			return true;
		}
		return false;
	}

	public void logout() {
		CurrentUser.logOut();
	}

	public List<User> getAllUsers() {
		List<User> userList = userDao.getAllUsers();
		return userList;
	}

	public List<User> getUsers(Integer perPage, Integer index) {
		List<User> userList = userDao.getUsers(perPage, index);
		return userList;
	}

	public User getUser(Integer id) {
		User user = userDao.getUser(id);
		return user;
	}

	public User getUser(String username) {
		User user = userDao.getUser(username);
		return user;
	}

	public void insertUser(User user) {
		userDao.insertUser(user);
	}

	public void insertUser_authorizationDao(User_authorization user_authorization) {
		user_authorizationDao.insertUser_authorization(user_authorization);
	}
}
