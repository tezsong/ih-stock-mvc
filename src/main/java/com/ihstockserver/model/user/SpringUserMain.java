package com.ihstockserver.model.user;

import com.ihstockserver.model.user.dto.CurrentUser;
import com.ihstockserver.model.user.dto.User;
import com.ihstockserver.model.user.dto.User_authorization;
import com.ihstockserver.model.user.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUserMain {
	static final Logger logger = LogManager.getLogger(SpringUserMain.class.getName());
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("/META-INF/spring/app-context.xml");

		UserService userService = context.getBean("userServiceImpl", UserService.class);

		CurrentUser.logIn("admin");

		User user = new User();
		user.setUsername("admin");
		user.setPassword("1");
		user.setEnabled(true);
		userService.insertUser(user);
		User_authorization user_authorization = new User_authorization();
		user_authorization.setId(1);
		user_authorization.setAuthority("ROLE_ADMIN");
		userService.insertUser(user);

		userService.login("admin", "1");
	}
}
