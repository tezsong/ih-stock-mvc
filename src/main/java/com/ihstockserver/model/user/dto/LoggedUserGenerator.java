package com.ihstockserver.model.user.dto;

import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

public class LoggedUserGenerator implements ValueGenerator<String> {

	@Override
	public String generateValue(Session session, Object owner) {
		return CurrentUser.INSTANCE.get();
	}
}
