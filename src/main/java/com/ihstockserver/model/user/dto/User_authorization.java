package com.ihstockserver.model.user.dto;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table
public class User_authorization {
  @Id
  private Integer id;
  private String authority;

  @GeneratorType(type = LoggedUserGenerator.class,
      when = GenerationTime.INSERT)
  private String createdBy;
  @GeneratorType(type = LoggedUserGenerator.class,
      when = GenerationTime.ALWAYS)
  private String updatedBy;
  @CreationTimestamp
  private Timestamp createdAt;
  @UpdateTimestamp
  private Timestamp updatedAt;

  public void setId(Integer id) {
    this.id = id;
  }
  public Integer getId() {
    return id;
  }
  public void setAuthority(String authority) {
    this.authority = authority;
  }
  public String getAuthority() {
    return authority;
  }
  public String getUpdatedBy() {
    return updatedBy;
  }
  public String getCreatedBy() {
    return createdBy;
  }
}
