package com.ihstockserver.model.user.dto;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String username;
//	@ColumnTransformer(
//			read = "AES_DECRYPT(password, 'cryptword') ",
//			write = "AES_ENCRYPT(?, 'cryptword') "
//	)
//	@ColumnTransformer(
//			write = "SHA2(?, 224) "
//	)
	private String password;

  private boolean enabled;

	@GeneratorType(type = LoggedUserGenerator.class,
			when = GenerationTime.INSERT)
	private String createdBy;
	@GeneratorType(type = LoggedUserGenerator.class,
			when = GenerationTime.ALWAYS)
	private String updatedBy;
	@CreationTimestamp
	private Timestamp createdAt;
	@UpdateTimestamp
	private Timestamp updatedAt;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User user = (User) obj;
		if (id != null ? !id.equals(user.id) : user.id != null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return "User [" +
				"id=" + id + ", " +
				"username=" + username + ", " +
				"password=" + password + ", " +
				"createdBy=" + createdBy + ", " +
				"updatedBy=" + updatedBy + ", " +
				"createdAt=" + createdAt + ", " +
				"updatedAt=" + updatedAt + "]";
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }
  public boolean getEnabled() {
    return enabled;
  }
}
