package com.ihstockserver.controller;

import com.ihstockserver.controller.news.NewsController;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.ModelMap;

public class NewsControllerJUnitTest {
  static final Logger logger = LogManager.getLogger(NewsControllerJUnitTest.class.getName());

  @Test
  public void test() {
    NewsController newsController = new NewsController();

    ModelMap modelMap = new ExtendedModelMap();

    String view = newsController.welcomeNews(modelMap);

    Assert.assertNotNull(view);
    Assert.assertEquals("news", view);

    String titlename = modelMap.get("title").toString();
    Assert.assertEquals("NEWS - ihsong", titlename);

    String name = modelMap.get("name").toString();
    Assert.assertEquals("Hellow World", name);

    String greetings = modelMap.get("greetings").toString();
    Assert.assertEquals("welcom to ih publishing - spring MVC", greetings);
  }
}
