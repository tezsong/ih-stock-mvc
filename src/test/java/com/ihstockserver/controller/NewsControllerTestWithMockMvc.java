package com.ihstockserver.controller;

import com.ihstockserver.controller.news.NewsController;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({
    "classpath*:/spring/applicationContext.xml",
    "classpath*:dispatcher-servlet.xml"
})
//@ContextConfiguration({"classpath:/WEB-INF/dispatcher-servlet.xml"})
//@ContextConfiguration({"classpath:/WEB-INF/dispatcher-servlet.xml"})
public class NewsControllerTestWithMockMvc {
  static final Logger logger = LogManager.getLogger(NewsControllerTestWithMockMvc.class.getName());

  @InjectMocks
  private NewsController newsController;
  private MockMvc mockMvc;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    this.mockMvc = MockMvcBuilders.standaloneSetup(newsController).build();
  }

  @Test
  public void testHome() throws Exception {
    mockMvc.perform(get("/news"))
        .andExpect(status().isOk())
        .andExpect(view().name("hello"))
        .andExpect(model().attribute("title", "NEWS - ihsong"))
        .andExpect(model().attribute("name", "Hellow World"))
        .andExpect(model().attribute("greetings", "welcom to ih publishing - spring MVC"));
  }
}
