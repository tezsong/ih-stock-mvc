package com.ihstockserver.model.user;

import com.ihstockserver.model.user.dto.CurrentUser;
import com.ihstockserver.model.user.dto.User;
import com.ihstockserver.model.user.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUserTestMain {
	static final Logger logger = LogManager.getLogger(SpringUserTestMain.class.getName());
	@Test
	public void test() {
		ApplicationContext context = new ClassPathXmlApplicationContext("/META-INF/spring/app-context.xml");

		UserService userService = context.getBean("userServiceImpl", UserService.class);

		CurrentUser.logIn("admin");

		User user = new User();
		user.setUsername("admin");
		user.setPassword("1");
		user.setEnabled(true);
//		user.setAuthority("ROLE_ADMIN");
		userService.insertUser(user);

		userService.login("admin", "1");

		user = new User();
		user.setUsername("tezsong");
		user.setPassword("0000000");
		user.setEnabled(true);
//		user.setAuthority("ROLE_USER");
		userService.insertUser(user);

		user.setUsername("katar84");
		user.setPassword("00001111");
		user.setEnabled(true);
//		user.setAuthority("ROLE_USER");
		userService.insertUser(user);

		for(User c: userService.getAllUsers()) {
			logger.debug(c);
		}

		user = userService.getUser(1);
		logger.debug(user);
		user = userService.getUser(2);
		logger.debug(user);

		user.setUsername("katar1");
		user.setPassword("aaaa0000");
		user.setEnabled(false);
//		user.setAuthority("ROLE_USER");
		userService.insertUser(user);

		user.setUsername("katar2");
		user.setPassword("0000AAAA");
		user.setEnabled(false);
//		user.setAuthority("ROLE_USER");
		userService.insertUser(user);

		user = userService.getUser(3);
		logger.debug(user);
		user = userService.getUser(4);
		logger.debug(user);
		user = userService.getUser(5);
		logger.debug(user);
	}
}
