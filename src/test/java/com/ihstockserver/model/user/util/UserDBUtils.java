package com.ihstockserver.model.user.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class UserDBUtils {
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void initialize() {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			statement.execute("DROP TABLE IF EXISTS User ");
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS User (" +
					"id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
					"username VARCHAR(10) NOT NULL, " +
//					"password VARBINARY(255) NOT NULL, " +
					"password VARCHAR(255) NOT NULL, " +
					"enabled SMALLINT NOT NULL, " +
					"authority VARCHAR(10) NOT NULL, " +
					"createdBy VARCHAR(10) NOT NULL," +
					"updatedBy VARCHAR(10) NULL, " +
					"createdAt TIMESTAMP NOT NULL, " +
					"updatedAt TIMESTAMP NULL, " +
					"UNIQUE INDEX username (username)" +
					")");

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS User_authorization (" +
					"id BIGINT(20) NOT NULL, " +
					"authority VARCHAR(10) NOT NULL, " +
					"createdBy VARCHAR(10) NOT NULL," +
					"updatedBy VARCHAR(10) NULL, " +
					"createdAt TIMESTAMP NOT NULL, " +
					"updatedAt TIMESTAMP NULL " +
//					"INDEX FK_USER (id)," +
//					"CONSTRAINT FK_USER FOREIGN KEY (id) REFERENCES User (id)" +
					")");

//			(createdAt, createdBy, username, password, pswd, updatedAt, updatedBy)
//			(?, ?, ?, AES_ENCRYPT(?, 'cryptword') , SHA2(?, 224) , ?, ?)
//				statement.executeUpdate("INSERT INTO User (id, username, password, pswd, enabled, authority, createdBy, createdAt) " +
//						"VALUES (1, 'admin', AES_ENCRYPT('1234','cryptword'), SHA2('1234',224), 1, 'ROLE_ADMIN', 'admin', NOW())");


			statement.close();
			connection.close();
		} catch(SQLException err) {
			err.printStackTrace();
		}
	}
}
