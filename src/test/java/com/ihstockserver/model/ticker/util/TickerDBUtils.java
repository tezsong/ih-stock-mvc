package com.ihstockserver.model.ticker.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class TickerDBUtils {
	static final Logger logger = LogManager.getLogger(TickerDBUtils.class.getName());
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void initialize() {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			statement.execute("DROP TABLE IF EXISTS Ticker");
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Ticker(" +
//					"id INT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
					"market varchar(10) NOT NULL, " +
					"code varchar(10) NOT NULL, " +
					"date char(10) NOT NULL," +
					"close INT(20) NOT NULL, " +
					"high INT(20) NOT NULL, " +
					"low INT(20) NOT NULL, " +
					"open INT(20) NOT NULL, " +
					"volume INT(20) NOT NULL, " +
					"PRIMARY KEY (code, date), " +
					"INDEX market (market)" +
					")");

			String sql = "INSERT INTO TIcker (market, code, date, close, high, low, open, volume) " +
					"VALUES ('kospi', '0000000', '1998-01-02', 4900, 5000, 4000, 4100, 1234567890)";
			logger.debug(sql);
			statement.executeUpdate(sql);

			statement.close();
			connection.close();
		} catch(SQLException err) {
			err.printStackTrace();
		}
	}
}
