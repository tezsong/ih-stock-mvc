package com.ihstockserver.model.ticker;

import com.ihstockserver.model.ticker.dto.Ticker;
import com.ihstockserver.model.ticker.service.TickerService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//import java.sql.Date;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;

public class SpringTickerTestMain {
	static final Logger logger = LogManager.getLogger(SpringTickerTestMain.class.getName());
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("/META-INF/spring/app-context.xml");

		TickerService tickerService = context.getBean("tickerServiceImpl", TickerService.class);

		Ticker tick = new Ticker();
//		tick.setId(2);
		tick.setMarket("kosdaq");
		tick.setCode("0000000");
		tick.setDate("2000-01-03");
		tick.setClose(4800);
		tick.setHigh(5000);
		tick.setLow(4000);
		tick.setOpen(4200);
		tick.setVolume(1234567890);
		tickerService.insertTicker(tick);

		tick.setMarket("kosdaq");
		tick.setCode("0000000");
		tick.setDate("2000-01-04");
		tick.setClose(4800);
		tick.setHigh(5000);
		tick.setLow(4000);
		tick.setOpen(4200);
		tick.setVolume(1234567890);
		tickerService.insertTicker(tick);

		for(Ticker ticker: tickerService.getAllTickers("0000000")) {
			logger.debug(ticker);
		}

		tick = tickerService.getTickers("0000000", 1, 0).get(0);
		logger.debug(tick);
		tick = tickerService.getTickers("0000000", 1, 1).get(0);
		logger.debug(tick);
		tick = tickerService.getTickers("0000000", 1, 2).get(0);
		logger.debug(tick);
	}
}
