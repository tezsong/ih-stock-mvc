package com.ihstockserver.model.news.service;

import com.ihstockserver.model.news.dao.NewsDao;
import com.ihstockserver.model.news.dto.News;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
  static final Logger logger = LogManager.getLogger(NewsServiceTest.class.getName());
  private NewsService newsService;
  private News oldNews;

  @Mock
  private NewsDao newsDao;

  @Before
  public void setUp() {
    newsService = new NewsServiceImpl(newsDao);

    oldNews = new News();
    oldNews.setCode("0000000");
    oldNews.setDate("1998-10-07");
    oldNews.insertSentence("old 1");
    oldNews.insertSentence("old 2");
  }

  @Test
  public void getNews() {
    when(newsDao.getNews(1)).thenReturn(oldNews);
    News news = newsService.getNews(1);
    Assert.assertEquals(news, oldNews);

    verify(newsDao).getNews(1);
    verifyNoMoreInteractions(newsDao);
  }

  @Test
  public void notGetNews() {
    when(newsDao.getNews(1)).thenReturn(null);
    News news = newsService.getNews(1);
    Assert.assertNotEquals(news, oldNews);

    verify(newsDao).getNews(1);
    verifyZeroInteractions(newsDao);
    verifyNoMoreInteractions(newsDao);
  }
}
