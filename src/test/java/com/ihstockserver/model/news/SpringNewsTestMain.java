package com.ihstockserver.model.news;

import com.ihstockserver.model.news.dto.News;
import com.ihstockserver.model.news.service.NewsService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringNewsTestMain {
  static final Logger logger = LogManager.getLogger(SpringNewsTestMain.class.getName());

  @Test
  public void test() {
    ApplicationContext context = new ClassPathXmlApplicationContext("/META-INF/spring/app-context.xml");

    NewsService newsService = context.getBean("newsServiceTestImpl", NewsService.class);

    News news = new News();
    news.setCode("0000000");
    news.setDate("2000-01-03");
    news.insertSentence("2016년 전 세계 리튬이차전지용 양극소재 출하량은 약 215,000톤 가량이며,");
    news.insertSentence("전기차와 에너지 저장장치, 전동공구 등 중대형 전지의 성장세에 힘입어 2020년 년간 출하량은 약 900,000톤으로 증가할 전망");
    newsService.insertNews(news);

    for(News n: newsService.getAllNews("0000000")) {
      logger.debug(n);
    }

    News result = newsService.getNews("0000000", 1, 0).get(0);
    logger.debug("result:" + result);
    assert news.equals(result);

    news.removeSentence("2016년 전 세계 리튬이차전지용 양극소재 출하량은 약 215,000톤 가량이며,");
    newsService.test(news);

    result = newsService.getNews("0000000", 1, 0).get(0);
    logger.debug("result:" + result);
    assert news.equals(result);
  }
}
