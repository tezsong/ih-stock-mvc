package com.ihstockserver.model.news.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class NewsDBUtils {
  @Autowired
  private DataSource dataSource;

  @PostConstruct
  public void initialize() {
    try {
      Connection connection = dataSource.getConnection();
      Statement statement = connection.createStatement();

      statement.execute("DROP TABLE IF EXISTS News_Sentence");
      statement.execute("DROP TABLE IF EXISTS Sentence");
      statement.execute("DROP TABLE IF EXISTS News");

      statement.executeUpdate("CREATE TABLE IF NOT EXISTS News (" +
          "id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
          "code VARCHAR(10) NOT NULL, " +
          "date CHAR(10) NOT NULL" +
          ")"
      );

      statement.executeUpdate("CREATE TABLE IF NOT EXISTS Sentence(" +
          "id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
          "code VARCHAR(10) NOT NULL, " +
          "date CHAR(10) NOT NULL," +
          "sentence VARCHAR(1000) NOT NULL, " +
          "news_id BIGINT(20) NOT NULL, " +
          "INDEX FK_NEWS (news_id)," +
          "CONSTRAINT FK_NEWS FOREIGN KEY (news_id) REFERENCES news (id)" +
          ")");

//      statement.executeUpdate("CREATE TABLE IF NOT EXISTS News_Sentence (" +
//          "News_id BIGINT(20) NOT NULL, " +
//          "Sentences_id BIGINT(20) NOT NULL, " +
//          "INDEX FK_NEWS (News_id)," +
//          "INDEX FK_SENTENCE (Sentences_id)," +
//          "CONSTRAINT FK_NEWS FOREIGN KEY (News_id) REFERENCES News (id), " +
//          "CONSTRAINT FK_SENTENCE FOREIGN KEY (Sentences_id) REFERENCES Sentence (id)" +
//          ")"
//      );

//      String sql = "INSERT INTO TIcker (market, code, date, close, high, low, open, volume) " +
//          "VALUES ('kospi', '0000000', '1998-01-02', 4900, 5000, 4000, 4100, 1234567890)";
//      logger.debug(sql);
//      statement.executeUpdate(sql);
      statement.close();
      connection.close();
    } catch(SQLException err) {
      err.printStackTrace();
    }
  }
}
