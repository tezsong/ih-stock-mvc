package com.ihstockserver.model.news.dao;

import com.ihstockserver.model.news.dto.News;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewsDtoTestWithInMemoryNewsDao {
  static final Logger logger = LogManager.getLogger(NewsDtoTestWithInMemoryNewsDao.class.getName());

  private News oldNews;
  private News newNews;
  private InMemoryNewsDaoImpl newsDao;

  @Before
  public void setUp() {
    oldNews = new News();
    oldNews.setCode("0000000");
    oldNews.setDate("1998-10-07");
    oldNews.insertSentence("old 1");
    oldNews.insertSentence("old 2");

    newsDao = new InMemoryNewsDaoImpl();
    newsDao.insertNews(oldNews);

    newNews = new News();
    newNews.setCode("0000000");
    newNews.setDate("2017-10-07");
    newNews.insertSentence("sen 1");
    newNews.insertSentence("sen 2");
  }

  @Test
  public void isOldNewsTest() {
    logger.debug("oldNews.getId():"+oldNews.getId());
    System.out.println("oldNews.getId():"+oldNews.getId());
    Assert.assertTrue(newsDao.isOldNews(oldNews.getId()));

    logger.debug("newNews.getId():"+newNews.getId());
    System.out.println("newNews.getId():"+newNews.getId());
    Assert.assertFalse(newsDao.isOldNews(newNews.getId()));
  }

  @Test
  public void createNewNewsTest() {
    newsDao.insertNews(newNews);

    logger.debug("newNews.getId():"+newNews.getId());
    System.out.println("newNews.getId():"+newNews.getId());
    Assert.assertTrue(newsDao.isOldNews(newNews.getId()));
  }

  @Test
  public void deleteNewsTest() {
    newsDao.deleteNews(oldNews);

    logger.debug("oldNews.getId():"+oldNews.getId());
    System.out.println("oldNews.getId():"+oldNews.getId());
    Assert.assertFalse(newsDao.isOldNews(oldNews.getId()));
  }
}
