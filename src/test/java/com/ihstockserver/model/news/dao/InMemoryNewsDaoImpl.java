package com.ihstockserver.model.news.dao;

import com.ihstockserver.model.news.dto.News;

import java.util.*;

public class InMemoryNewsDaoImpl implements NewsDao {
  private Map<Integer, News> newsMap;
  Integer id_auto_increment = 0;

  public InMemoryNewsDaoImpl() {
    newsMap = Collections.synchronizedMap(new HashMap<Integer, News>());
  }

  public boolean isOldNews(Integer id) {
    return newsMap.containsKey(id);
  }

  @Override
  public List<News> getAllNews(String code) {
    List<News> news = new ArrayList<>();
    for(Integer key: newsMap.keySet()) {
      news.add(newsMap.get(key));
    }
    return news;
  }

  @Override
  public News getNews(Integer id) {
    return newsMap.get(id);
  }

  @Override
  public List<News> getNews(String code, Integer perPage, Integer index) {
    List<News> news = new ArrayList<>();
    Integer i = 0;
    for(Integer key: newsMap.keySet()) {
      if (i >= perPage*index && i < perPage*(index+1)) {
        news.add(newsMap.get(key));
      }
      i++;
    }

    return news;
  }

  @Override
  public void insertNews(News news) {
    if (news.getId() == null) {
      ++id_auto_increment;
      news.setId(id_auto_increment);
    }
    if (!isOldNews(news.getId())) {
      newsMap.put(news.getId(), news);
    }
  }

  @Override
  public void updateNews(News news) {
    if (!isOldNews(news.getId())) {
      newsMap.put(news.getId(), news);
    }
  }

  @Override
  public void deleteNews(News news) {
    if (isOldNews(news.getId())) {
      newsMap.remove(news.getId());
    }
  }

  @Override
  public void deleteNews(Integer newsId) {
    if (!isOldNews(newsId)) {
      newsMap.remove(newsId);
    }
  }

  public void test(News news) {}
}
