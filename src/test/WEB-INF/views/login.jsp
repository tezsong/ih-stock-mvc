<%--
  Created by IntelliJ IDEA.
  User: TerrorToy
  Date: 2017-10-05
  Time: 오후 3:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Login</title>
</head>
<body>
  <form action="/j_spring_security_check" method="post">
    <table>
      <tr>
        <td>userName:</td>
        <td><input type="text" name="j_username" value=""></td>
      </tr>
      <tr>
        <td>password:</td>
        <td><input type="password" name="j_password"></td>
      </tr>
      <tr>
        <td>Remember me:</td>
        <td><input type="checkbox" name="_spring_security_remember_me"></td>
      </tr>
      <tr>
        <td><input name="submit" type="submit" value="submit"></td>
      </tr>
    </table>
  </form>
</body>
</html>
