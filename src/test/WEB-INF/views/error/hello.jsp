<%--
  Created by IntelliJ IDEA.
  User: TerrorToy
  Date: 2017-10-05
  Time: 오전 11:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Exceptions!</title>
</head>
<body>
  <h1 style="color: red">Sorry! Unable to process current Request</h1>
  <b>${error}</b>: ${message}
</body>
</html>
