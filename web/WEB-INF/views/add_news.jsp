<%--
  Created by IntelliJ IDEA.
  User: ihsong
  Date: 2017-09-19
  Time: 오후 2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Add NEWS</title>
  </head>
  <body>
    <div align="center">
      Add NEWS page
    </div>
    <div align="center">
      <table cellspacing="0" cellpadding="6" border="1">
        <tr>
          <td align="right">NEWS Information</td>
        </tr>
        <tr>
          <form:form method="post" action="update_news">
            <table width="100%">
              <tr>
                <td>
                  <form:label path="code">code</form:label>
                </td>
                <td align="left" width="70%">
                  <form:input path="code"/>
                </td>
              </tr>
              <tr>
                <td>
                  <form:label path="date">date</form:label>
                </td>
                <td align="left" width="70%">
                  <form:input path="date"/>
                </td>
              </tr>
              <tr>
                <td colspan="2"><input type="submit" value="Submit" /></td>
              </tr>
            </table>
          </form:form>
        </tr>
      </table>
    </div>
  </body>
</html>
