<%--
  Created by IntelliJ IDEA.
  User: ihsong
  Date: 2017-09-19
  Time: 오후 2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>${Title}</title>
  </head>
  <body>
    <div align="center">
      News List
    </div>
    <div align="center">
      <table cellspacing="0" cellpadding="6" border="1">
        <tr>
          <td align="right"><a href="${pageContext.request.contextPath}/news/add_news">Add NEWS</a></td>
        </tr>
        <tr>
          <th>No</th>
          <th>NEWS</th>
          <th>delete</th>
        </tr>
        <c:forEach var="item" items="${list}" varStatus="status">
          <tr>
            <td>=${status.index+1}</td>
            <td>-${item}</td>
            <td>-<a href="${pageContext.request.contextPath}/news/delete_news/${item.id}">Delete</a></td>
          </tr>
        </c:forEach>
      </table>
    </div>
  </body>
</html>
