<%--
  Created by IntelliJ IDEA.
  User: ihsong
  Date: 2017-09-19
  Time: 오후 2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$ h</title>
  </head>
  <body>
    <div align="center">
      <table cellspacing="0" cellpadding="6" border="1">
        <c:forEach var="item" items="${list}" varStatus="status">
          <tr>
            <td>-${ticker}</td>
            <td>+${status.index}</td>
            <td>+${status.count}</td>
            <td>open${item.open}</td>
            <td>close${item.close}</td>
            <td>date${item.date}</td>
          </tr>
        </c:forEach>
      </table>
    </div>
  </body>
</html>
